<?php
/**
 * Created by PhpStorm.
 * User: alexneyra
 * Date: 23/10/18
 * Time: 08:29 PM
 */

Route::options('{all}', function () {
    return response('ok', 200)
        ->header('Access-Control-Allow-Credentials', 'true')
        ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
        ->header('Access-Control-Allow-Headers', 'Content-Type, Accept, Authorization, X-Requested-With')
        ->header('Access-Control-Allow-Origin', '*');
})->where('all', '.*');




Route::post('/users/login/', 'UserController@login');

Route::group(['middleware' => 'jwt.auth'], function() {
    Route::get('/documents/', 'CPEController@index');
    Route::get('/documents/counter/', 'CPEController@counter');
    Route::get('/documents/voucher-type/', 'VoucherTypeController@findAll');
    Route::get('/documents/series/', 'CPEController@series');
    Route::get('/documents/customers/', 'CPEController@customers');
    Route::get('/documents/details/', 'CPEController@details');
    Route::get('/users/', 'UserController@findAll');
    Route::post('/users/', 'UserController@create');
    Route::post('/users/enterprise', 'UserController@saveUserEnterprise');
    Route::delete('/users/enterprise/{id}', 'UserController@deleteUserEnterprise');
    Route::put('/users/password/{userId}', 'UserController@updatePassword');
    Route::get('/users/info/', 'UserController@info');
    Route::post('/enterprises', 'EnterpriseController@create');
    Route::get('/users/enterprise/access/{ruc}', 'UserController@hasAccessEnterprise');
    Route::get('/users/enterprise/{ruc}', 'UserController@findByEnterprise');
    Route::put('/users/enterprise/{id}', 'UserController@updateEnterpriseByUser');
    Route::get('/users/notadded/{ruc}', 'UserController@findUserNotAdded');
    Route::get('/enterprises/user', 'EnterpriseController@findByUser');
    Route::get('/enterprises/', 'EnterpriseController@findAll');
});
