<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;


class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{

            JWTAuth::parseToken()->authenticate();


        }catch (\Exception $e){
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['error'=>'Sesión Inválidad'], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['error'=>'Su sesión a caducado'], 401);
            }else{
                return response()->json(['error'=>'Usuario no auntentificado'], 401);
            }

        }
        return $next($request);
    }
}
