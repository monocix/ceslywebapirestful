<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Common\Helpers;
use App\User;
use App\UserEnterprise;
use App\Http\Requests;
use Illuminate\Http\Response\response;
use JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Ramsey\Uuid\Uuid;
use DB;

class UserController extends Controller
{

    public function login(Request $request){
        $data = $request->json()->all();

        $user = User::where("nombre", $data ['username'])
                ->where('clave', md5($data['password']))
                ->get();
                
        if ($user->isEmpty()) {
            return response()->json(['error' => 'Clave ingresada no es correcta'], 401);
        };

        $payload = JWTFactory::sub("Token")->aud($data ['username'])->user($user)->make();
        $token = JWTAuth::encode($payload);

        return response()->json([
            "id" => strtoupper(Uuid::uuid4()->toString()),
            "success"=> "Token generado con exito",
            "token" => $token->get()
        ], 200);
    }

    public function hasAccessEnterprise($ruc)
    {
        $tokenUser = Helpers::getUserJwt();
        $user = User::where('id', $tokenUser->id)->first();
        $enterprise = $user->emisors()
            ->where('ruc', $ruc)
            ->get();
        if (count($enterprise) == 0) {
            return response()->json(['error'=> 'Usuario no tiene acceso a esta empresa'], 401);
        }

        $payload = JWTFactory::sub("Token")->aud($user->nombre)->user($enterprise)->make();
        $token = JWTAuth::encode($payload);

        return response()->json([
            "id" => strtoupper(Uuid::uuid4()->toString()),
            "success"=> "Token generado con exito",
            "token" => $token->get()
        ], 200);

    }

    public function findByEnterprise($ruc)
    {
        Helpers::validToken();
        $enterprises = DB::table('usuario_emisor')
            ->join('usuario', 'usuario_emisor.usuario_id', '=', 'usuario.id')
            ->select('usuario.id as id', 'usuario.nombre as username', 
                'usuario.estado as status', 'usuario.nivel as level',
                'usuario_emisor.id as userEnterpriseId')
            ->where('usuario_emisor.ruc', $ruc)
            ->get();

    return response()->json($enterprises, 200);

    }

    public function create(Request $request) 
    {
        $user = Helpers::getUserJwt();


        if ($user->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a crear usuarios'], 401);
        }

        $data = $request->json()->all();

        if (User::where('nombre', $data['nombre'])->first() !== null) {
            return response()->json(['error'=> 'Usuario ya se encuentra registrado'], 409);
        }

        $data['clave'] = md5('peru123');
        $data['nivel'] = 9;
        $data['estado'] = 0;

        $user = User::create($data);
        $userJson = [
            'id' => $user->id.'',
            'status' => '0',
            'level' => $user->nivel.'',
            'username' => $user->nombre
        ];
    
        return response()->json($userJson, 200);
    }

    public function findAll() 
    {
        Helpers::validToken();
        
        return response()->json(User::all(), 200);
    }


    public function updatePassword($userId) 
    {
        $userToken = Helpers::getUserJwt();

        if ($userToken->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a este recurso'], 401);
        }


        
        User::where('id', $userId)
            ->update(['clave'=> md5('peru123')]);
        
        return response()->json([
            "success"=> "Recurso actualizado con exito",
        ], 200);
    }

    public function updateEnterpriseByUser(Request $request, $id) {
        $userToken = Helpers::getUserJwt();

        if ($userToken->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a este recurso'], 401);
        }

        $data = $request->json()->all();
        UserEnterprise::where('usuario_id', $id)->delete();

        $params = [];

        for ($i=0; $i < count($data); $i++) { 
            $params[] = ['usuario_id'=>$id, 'ruc'=>$data[$i]];
        }

        UserEnterprise::insert($params);

        return response()->json([
            "success"=> "Recurso actualizado con exito",
        ], 200);
    }

    public function findUserNotAdded($ruc)
    {
        Helpers::validToken();
        $users = DB::table('usuario')
            ->select('usuario.id', 'usuario.nombre as username', 'usuario.nivel as level', 'usuario.estado as status')
            ->whereNotIn('id', function($q) use($ruc){
                $q->select('usuario_id')
                    ->from('usuario_emisor')
                    ->where('ruc', '=', $ruc);
            })            
            ->get();
        return response()->json($users, 200);
    }

    public function saveUserEnterprise(Request $request)
    {
        $user = Helpers::getUserJwt();

        if ($user->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a crear usuarios'], 401);
        }

        $data = $request->json()->all();
        UserEnterprise::insert($data);
        return response()->json(['success' => 'Usuario agregado'], 201);

    }

    public function deleteUserEnterprise($id)
    {
        $user = Helpers::getUserJwt();

        if ($user->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a crear usuarios'], 401);
        }

        UserEnterprise::where('id', $id)->delete();

        return response()->json(['success' => 'Usuario eliminado'], 200);

    }

}


