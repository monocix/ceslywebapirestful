<?php
namespace App\Http\Controllers;

use App\Common\Helpers;
use Illuminate\Http\Request;
use App\CPE;
use DB;
use App\Enterprise;


class EnterpriseController extends Controller
{
    public function findByUser()
    {
        $user = Helpers::getUserJwt();
        $id = $user->id;

        $enterprises = DB::table('usuario_emisor')
            ->join('emisor', 'usuario_emisor.ruc', '=', 'emisor.emi_ruc')
            ->select('emisor.emi_ruc as ruc', 'emisor.emi_razon_social as enterpriseName',
                'emisor.url_web as url', 'emisor.logo as image')
            ->where('usuario_emisor.usuario_id', $id)
            ->get();

        return response()->json($enterprises, 200);
    }

    public function findAll()
    {
        $user = Helpers::getUserJwt();
        if ($user->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a este recurso'], 401);
        }

        $enterprises = DB::table('emisor')
            ->select('emisor.emi_ruc as ruc', 'emisor.emi_razon_social as enterpriseName',
                'emisor.url_web as url', 'emisor.logo as image')
            ->get();

        return response()->json($enterprises, 200);
    }

    public function create(Request $request)
    {
        
        $user = Helpers::getUserJwt();

        if ($user->nivel > 1) {
            return response()->json(['error'=> 'Usuario no tiene acceso a crear usuarios'], 401);
        }

        if (Enterprise::where('emi_ruc', $data['emi_ruc'])->first() !== null) {
            return response()->json(['error'=> 'Emisor ya se encuentra registrado'], 409);
        }

        $data = $request->json()->all();

        
        $enterprise = Enterprice::create($data);

        return response()->json($enterprise, 200);
    }
}