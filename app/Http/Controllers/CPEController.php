<?php

namespace App\Http\Controllers;

use App\Common\Helpers;
use Illuminate\Http\Request;
use App\CPE;
use DB;

class CPEController extends Controller
{
    
    public function index(Request $request)
    {
        $user = Helpers::getUserJwt();
        $table = 'cpe_'.$user->ruc;

        $from = $request->input('from');
        $to = $request->input('to');
        $voucherTypes = $request->input('voucherTypes');        
        $serieNumbers = $request->input('serieNumbers');
        $customerNames = $request->input('customerNames');

        $documents = DB::table($table)
            ->select(DB::raw(
                    "UPPER(UUID()) as id,".
                    "tipodoc as voucherType,".
                    "nser as serieNumber,".
                    "ndoc as documentNumber,".
                    "fecha as dateIssue,".
                    "num_doc_ident_clie as documentIdentity,".
                    "nomb_clie as customerName,".
                    "mone as coin,".
                    "total_venta_ope_grav as taxedAmount,".
                    "total_venta_ope_exo as exoneratedAmount,".
                    "total_igv as taxAmount,".
                    "total_doc as totalAmount,".
                    "estado as state,".
                    "estado_cpe as stateCpe"
            ))
            ->where("fecha", ">=", $from)
            ->where("fecha", "<=", $to);
        
        if (!is_null($voucherTypes)) {
            $documents = $documents->whereIn("tipodoc", $voucherTypes);
        }

        if (!is_null($serieNumbers)) {
            $documents = $documents->whereIn("nser", $serieNumbers);
        }

        if (!is_null($customerNames)) {
            $documents = $documents->whereIn("nomb_clie", $customerNames);
        }

        $documents = $documents->paginate(50);

        return response()->json($documents, 200);
    }

    public function counter(){
        $user = Helpers::getUserJwt();
        $table = 'cpe_'.$user->ruc;
        $counters = DB::table($table)
            ->join('documento', $table.'.tipodoc', '=', 'documento.tipodoc')
            ->select(DB::raw('UPPER(UUID()) as id, count(*) as quantity, documento.tipodoc as voucherCode, documento.nombre as voucherName'))
            ->where("estado", "1")
            ->groupBy("documento.tipodoc", "documento.nombre")
            ->get();

       return response()->json($counters, 200);
    }

    public function series()
    {
        $user = Helpers::getUserJwt();
        $table = 'cpe_'.$user->ruc;
        $counters = DB::table($table)
            ->select(DB::raw('UPPER(UUID()) as id, nser as serieNumber'))
            ->distinct()
            ->get();

       return response()->json($counters, 200);
    }

    public function customers()
    {
        $user = Helpers::getUserJwt();
        $table = 'cpe_'.$user->ruc;
        $counters = DB::table($table)
            ->select(DB::raw('UPPER(UUID()) as id, nomb_clie as customerName, codclie as code'))
            ->distinct()
            ->get();

       return response()->json($counters, 200);
    }

    public function details(Request $request)
    {
        $user = Helpers::getUserJwt();
        $table = 'cpe_det_'.$user->ruc;        
        $voucherType = $request->input('voucherType');        
        $serieNumber = $request->input('serieNumber');
        $correlative = $request->input('correlative');
        $detail = DB::table($table)
            ->select(DB::raw(
                "UPPER(UUID()) as id,".
                "codarti as productCode,".
                "descri as productName,".
                "um_vta as unitMeasurement,".
                "cant as quantity,".
                "prec as price,".
                "sub_total as  amount"
            ))
            ->where("tipodoc", $voucherType)
            ->where("nser", $serieNumber)
            ->where("ndoc", $correlative)
            ->get();

        return response()->json($detail, 200);
    }
}
