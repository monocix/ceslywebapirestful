<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
Use App\VoucherType;
use DB;

class VoucherTypeController extends Controller
{
    public function findAll()
    {
        $voucherTypes = DB::table('documento')
        ->select(DB::raw('UPPER(UUID()) as id, documento.tipodoc as voucherCode, documento.nombre as voucherName'))
        ->get();

        return response()->json($voucherTypes, 200);
    }
}
