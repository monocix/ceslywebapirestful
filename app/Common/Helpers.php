<?php
/**
 * Created by PhpStorm.
 * User: alexneyra
 * Date: 02/11/18
 * Time: 04:27 PM
 */

namespace App\Common;
use JWTAuth;

class Helpers
{
    public static function getUserJwt()
    {

        return json_decode(json_encode(JWTAuth::getPayload()['user'][0], JSON_FORCE_OBJECT));
    }

    public static function validToken(){
        JWTAuth::getPayload();
    }
}
