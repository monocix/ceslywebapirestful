<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEnterprise extends Model
{
    protected $table = 'usuario_emisor';

    protected $hidden = [ 'created_at', 'updated_at'];

}
