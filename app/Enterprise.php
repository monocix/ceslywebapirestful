<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    protected $table = 'emisor';
    protected $primaryKey = 'emi_ruc';
    public $timestamps = false;

    protected $fillable = [
        'emi_ruc',
        'emi_razon_social', 
        'emi_nombre_comercial',
        'emi_direccion',
        'emi_ubigeo',
        'url_web',
        'logo',
        'nro_reg_mtc',
        'es_molino',
        'modalidad_sis',
        'tipo_facturacion',
        'estado',
        'debe',
        'dias_gracia',
        'dia_vencimiento',
        'fecha_inicio_contrato',
        'nivel_sistema'
    ];
}
