<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmisorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emisor', function (Blueprint $table) {
            $table->string('emi_ruc', 11)->unique();
            $table->string('emi_razon_social', 150)->nullable();
            $table->string('emi_nombre_comercial', 150)->nullable();
            $table->text('emi_direccion')->nullable();
            $table->string('emi_ubigeo', 6)->nullable();
            $table->string('digital_file', 300)->nullable();
            $table->string('digital_pwd', 12)->nullable();
            $table->string('digital_razonsocial_resp', 150)->nullable();
            $table->string('digital_ruc_resp', 11)->nullable();
            $table->string('sol_usuario', 30)->nullable();
            $table->string('sol_pwd', 12)->nullable();
            $table->string('codigo_pais_cliente_def', 2)->nullable();
            $table->string('url_web', 120)->nullable();
            $table->string('nautoriza_sunat', 50)->nullable();
            $table->text('logo')->nullable();
            $table->text('nro_reg_mtc', 30)->nullable();
            $table->char('es_molino', 1)->nullable();
            $table->char('modalidad_sis', 1)->nullable();
            $table->char('tipo_facturacion', 1)->nullable();
            $table->char('estado', 1)->nullable();
            $table->char('debe', 1)->nullable();
            $table->integer('dias_gracia')->nullable();
            $table->integer('dia_vencimiento')->nullable();
            $table->date('fecha_deuda')->nullable();;
            $table->date('fecha_ultimo_pago')->nullable();
            $table->date('fecha_inicio_contrato')->nullable();
            $table->char('nivel_sistema', 1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emisor');
    }
}
