<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpeDet00000000000Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpe_det_00000000000', function (Blueprint $table) {
            $table->string('codarti', 6)->nullable();
            $table->decimal('item', 2, 0)->nullable();
            $table->string('tipodoc', 2)->nullable();
            $table->string('nser', 4)->nullable();
            $table->string('ndoc', 8)->nullable();
            $table->decimal('cant', 12, 3)->nullable();
            $table->decimal('prec', 12, 6)->nullable();
            $table->decimal('precio_ref', 12, 2)->nullable();
            $table->decimal('dcto_linea', 12, 2)->nullable();
            $table->string('cod_tipo_igv', 2)->nullable();
            $table->decimal('total_igv', 12, 6)->nullable();
            $table->string('descri', 250)->nullable();
            $table->decimal('importe', 14, 2)->nullable();
            $table->decimal('sub_total', 14, 2)->nullable();
            $table->string('cod_unid1', 3)->nullable();
            $table->decimal('peso', 12, 2)->nullable();
            $table->string('um_vta', 6)->nullable();
            $table->string('orden_compra', 12)->nullable();
            $table->decimal('cant2', 14, 4)->nullable();
            $table->decimal('prec2', 14, 6)->nullable();
            $table->string('unimed', 6)->nullable();
            $table->string('placa', 16)->nullable();
            $table->date('fecreg')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
