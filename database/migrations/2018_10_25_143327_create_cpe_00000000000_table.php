<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpe00000000000Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpe_00000000000', function (Blueprint $table) {
            $table->string('tipodoc', 2);
            $table->string('nser', 4);
            $table->string('ndoc', 8);
            $table->date('fecha')->nullable();
            $table->decimal('total_venta_ope_grav', 12, 2)->default(0.00);
            $table->decimal('total_venta_ope_ina', 12, 2)->default(0.00);
            $table->decimal('total_venta_ope_exo', 12, 2)->default(0.00);
            $table->decimal('total_venta_ope_grat', 12, 2)->default(0.00);
            $table->decimal('total_igv', 12, 2)->default(0.00);
            $table->decimal('porcigv', 5, 2)->default(0.00);
            $table->decimal('total_doc', 12, 2)->default(0.00);
            $table->decimal('dcto_global', 12, 2)->default(0.00);
            $table->char('mone', 1)->nullable();
            $table->string('nguia', 15)->nullable();
            $table->string('tipodoc2', 2)->nullable();
            $table->string('nser2', 4)->nullable();
            $table->string('ndoc2', 8)->nullable();
            $table->string('descri_motivo_nota', 150);
            $table->string('codmoti', 2)->nullable();
            $table->string('cod_moti_nde', 2)->nullable();
            $table->decimal('valor_venta', 14, 2)->default(0.00);
            $table->text('hash')->nullable();
            $table->char('estado', 1)->default('1');
            $table->char('estado_cpe', 1)->nullable();
            $table->integer('cod_rpta_cpe')->nullable();
            $table->char('estado_baja_cpe', 1)->nullable();
            $table->text('codezip');
            $table->string('nomb_clie', 70);
            $table->string('direccion_clie', 200)->nullable();
            $table->char('cod_tipo_doc_ide_clie', 2)->nullable();
            $table->string('num_doc_ident_clie', 20)->nullable();
            $table->char('cod_pais_clie', 2)->default('PE');
            $table->timestamp('fecreg');
            $table->string('codsuc', 2)->nullable();
            $table->string('dir_suc', 200)->nullable();
            $table->string('desc_ubig_suc', 150)->nullable();
            $table->char('es_fact_guia', 1)->default('0');
            $table->string('codclie', 10)->nullable();
            $table->string('pedido', 15)->nullable();
            $table->string('opci', 2)->nullable();
            $table->string('condicion', 40)->nullable();
            $table->decimal('total_dcto', 12, 2)->default(0.00);
            $table->decimal('total_percep', 12, 2)->default(0.00);
            $table->decimal('total_afecto_percep', 12, 2)->default(0.00);
            $table->decimal('porc_percep', 12, 2)->default(0.00);
            $table->date('fecha_venc')->nullable();
            $table->date('fecha_ini_trasl' )->nullable();
            $table->string('secuencia', 6)->nullable();
            $table->string('reparto', 10)->nullable();
            $table->string('codvend', 10)->nullable();
            $table->string('vendedor', 100)->nullable();
            $table->string('codzona', 6)->nullable();
            $table->string('zona', 70)->nullable();
            $table->char('vta_gratis', 1)->default('0');
            $table->char('vta_amaz', 1)->default('0');
            $table->char('tipo_vta', 1)->default('B');
            $table->string('partida', 250)->nullable();
            $table->string('llegada', 250)->nullable();
            $table->string('transportista', 150)->nullable();
            $table->string('ruc_transp', 14)->nullable();
            $table->string('marca_veh', 30)->nullable();
            $table->string('placa_veh', 20)->nullable();
            $table->string('const_veh', 30)->nullable();
            $table->string('licencia', 20)->nullable();
            $table->string('conductor', 100)->nullable();
            $table->decimal('peso_bruto', 12, 2)->default(0.00);
            $table->char('tipoimpu_vta', 1)->default('G');
            $table->string('orden_compra', 15)->nullable();
            $table->string('placa', 20)->nullable();
            $table->char('sello_ivap', 1)->default('0');
            $table->string('const_detr', 33)->nullable();
            $table->decimal('monto_detr', 12, 2)->nullable()->default(0.00);
            $table->char('exportacion', 1)->default('N');
            $table->decimal('detrac_val_ref', 14, 2)->nullable();
            $table->decimal('detrac_porc', 6, 2)->nullable();
            $table->string('detrac_num_cta', 25)->nullable();
            $table->string('detrac_cod_bb_ss', 3)->nullable();
            $table->decimal('detrac_tot_afec', 14, 2)->nullable();
            $table->decimal('detrac_tot', 14, 2)->nullable();
            $table->char('imp_con_igv', 1)->default('S');
            $table->char('anticipo', 1)->default('N');
            $table->string('detalle', 250)->nullable();
            $table->string('afecto_ivap', 1)->nullable()->default('N');
            $table->decimal('monto_ivap', 10, 2)->nullable();
            $table->string('codanexo', 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
